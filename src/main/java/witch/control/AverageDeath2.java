package witch.control;

import witch.rules.Person;

public class AverageDeath2 implements Person {
	int people1 =0;
	int people2 =0;
	@Override
	public int peopleKilled1(int yearOfDeath, int ageOfDeath) {
		// TODO Auto-generated method stub
		int bornOnYear = yearOfDeath-ageOfDeath;
		this.people1 = bornOnYear*2;
		return (this.people1);
	}
	
	@Override
	public int peopleKilled2(int yearOfDeath, int ageOfDeath) {
		// TODO Auto-generated method stub
		int bornOnYear = yearOfDeath-ageOfDeath;
		this.people2 = bornOnYear*2;
		return (this.people2);
	}
	
	@Override
	public int averageKilled() {
		// TODO Auto-generated method stub
		return (this.people1+this.people2)/2;
	}

	@Override
	public int ruleKiller() {
		// TODO Auto-generated method stub
		return 0;
	}

	

}

package witch.control;

import java.util.ArrayList;
import java.util.List;

import witch.rules.AverageDeath;

public class AverageDeathControl implements AverageDeath {
	List ageOfDeath = new ArrayList();
	List yearOfDeath = new ArrayList();
	List bornOnYear = new ArrayList();
	List peopleKilledOnYears = new ArrayList();
	
	public void people(int ageOfDeath, int yearOfDeath, int peopleOn) {
		this.ageOfDeath.add(peopleOn, ageOfDeath);
		this.yearOfDeath.add(peopleOn, yearOfDeath);
		this.bornOnYear.add(peopleOn, (yearOfDeath-ageOfDeath));
		this.peopleKilledOnYears(peopleOn);
	}

	@Override
	public int getAgeOfDeath(int num) {
		return (int) this.ageOfDeath.get(num);
	}

	@Override
	public int getYearOfDeath(int num) {
		return (int) this.yearOfDeath.get(num);
	}

	@Override
	public int getBornOnYear(int num) {
		return (int) this.bornOnYear.get(num);
	}

	@Override
	public int peopleKilledOnYears(int num) {
		int n = (int) this.bornOnYear.get(num);
		int sum = 1;
		for(int i=1;i<n;i++) {sum = sum+i;}
		this.peopleKilledOnYears.add(num, sum);
		return sum;
	}

	@Override
	public Double averagePeopleKilled() {
		int avgtemp = 0;
		for (int i = 0; i < this.peopleKilledOnYears.size(); i++) {
			avgtemp = avgtemp+(int)this.peopleKilledOnYears.get(i);
        }
		Double avg = (double) ((double)avgtemp/(double)2);
		// TODO Auto-generated method stub
		return avg;
	}


}

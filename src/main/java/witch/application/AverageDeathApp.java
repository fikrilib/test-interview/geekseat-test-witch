package witch.application;
import witch.control.AverageDeathControl;
import witch.util.ValidationException;
import witch.util.ValidationUtil;

public class AverageDeathApp extends AverageDeathControl {
	public static void main(String[] args) {
		AverageDeathControl AverageDeath = new AverageDeathControl();
		AverageDeath.people(10, 12, 0);
		AverageDeath.people(13, 17, 1);
		try {
	      ValidationUtil.validate(AverageDeath,0);
	      ValidationUtil.validate(AverageDeath,1);
	      
	      System.out.println(AverageDeath.averagePeopleKilled());
	    } catch (ValidationException exception) {
	      System.out.println("-1");
	    }
		
		
	}
}

package witch.application;

import witch.control.AverageDeath2;

public class AverageDeathApp2 {
	public static void main(String[] args) {
		AverageDeath2 person = new AverageDeath2();
		System.out.println(person.peopleKilled1(12,10));
		System.out.println(person.peopleKilled2(12,10));
		System.out.println(person.averageKilled());
	}
}
